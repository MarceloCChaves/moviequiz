import React, { useState } from "react";
import { TouchableOpacity, Image, StyleSheet, Text } from "react-native";

interface Props {
  opcoes: Filme[];
  update(): void;
  usados(filme: string): void;
}

interface Filme {
  Poster: string;
  Title: string;
}

const Card = ({ opcoes, update, usados }: Props) => {
  let sort: Filme = opcoes[Math.floor(Math.random() * opcoes.length)];
  const [pont, setPont] = useState<number>(0);
  const [rodada, setRodada] = useState<number>(0);

  const escolherFilme = (title: string) => {
    if (title === sort.Title) {
      setPont(pont + 1);
      update();
      usados(sort.Title);
      setRodada(rodada + 1);
    } else {
      update();
      usados(sort.Title);
      setRodada(rodada + 1);
    }
  };

  if (sort != undefined && rodada !== 5) {
    return (
      <>
        <Image source={{ uri: sort.Poster }} style={styles.poster} />
        <TouchableOpacity
          onPress={() => escolherFilme(opcoes[0].Title)}
          style={styles.title}
        >
          <Text style={{ color: "#FFFFFF" }}>{opcoes[0].Title}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => escolherFilme(opcoes[1].Title)}
          style={styles.title}
        >
          <Text style={{ color: "#FFFFFF" }}>{opcoes[1].Title}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => escolherFilme(opcoes[2].Title)}
          style={styles.title}
        >
          <Text style={{ color: "#FFFFFF" }}>{opcoes[2].Title}</Text>
        </TouchableOpacity>
      </>
    );
  } else {
    if(pont <= 2) {
      return(
        <>
          <Image source={{ uri: "https://c.tenor.com/U9VeSnXsUkAAAAAd/crying-man-sad.gif"}} style={styles.poster} />
          <Text style={{ color: "red", fontSize: 24 }}>Sua pontuação foi : {pont}</Text>
          <Text style={{ color: "red", fontSize: 36 }}>Você foi reprovado</Text>
        </>
      )
    }
    if(pont === 3){
      return(
        <>
          <Image source={{ uri: "https://64.media.tumblr.com/tumblr_ljrypgMUWN1qixleeo1_250.gifv"}} style={styles.poster} />
          <Text style={{ color: "yellow", fontSize: 24 }}>Sua pontuação foi : {pont}</Text>
          <Text style={{ color: "yellow", fontSize: 36 }}>Você ficou de recuperação</Text>
        </>
      )
    }
    if(pont >= 4){
      return (
        <>
          <Image source={{ uri: "https://r1.community.samsung.com/t5/image/serverpage/image-id/4683197i12B27D1DA5C0F87C/image-size/medium?v=v2&px=400"}} style={styles.poster} />
          <Text style={{ color: "green", fontSize: 24 }}>Sua pontuação foi : {pont}</Text>
          <Text style={{ color: "green", fontSize: 36 }}>Você foi aprovado</Text>
        </>
      );
    }
  }
};

const styles = StyleSheet.create({
  poster: {
    width: 300,
    height: 400,
  },
  title: {
    marginTop: 10,
    alignItems: "center",
    backgroundColor: "#0000FF",
    padding: 10,
  },
});

export default Card;
